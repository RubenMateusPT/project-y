﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundFix : MonoBehaviour
{
    [SerializeField] private AudioClip footstepsClip;

    private AudioSource audioSource;
    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    public void FootStepsAnimationEvent()
    {
        audioSource.PlayOneShot(footstepsClip);
    }
}
