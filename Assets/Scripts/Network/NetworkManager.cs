﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    public static NetworkManager Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    #region Master Sever Connection

    public void ConnectToServer()
    {
        PhotonNetwork.ConnectUsingSettings();
        PhotonNetwork.GameVersion = Application.version;
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Connected to Master Server");
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log($"Disconnect to Master Server. Reason: {cause.ToString()}");
    }

    #endregion

    #region Create Room

    public void CreateRoom()
    {
        PhotonNetwork.CreateRoom(
            CreateRoomCode(),
            new RoomOptions()
            {
                MaxPlayers = 2
            }
        );
    }

    private string CreateRoomCode()
    {
        Guid uuid = Guid.NewGuid();
        return uuid.ToString().Substring(0, 5).ToUpper();
    }

    #endregion

    #region Join Room

    public void JoinRoom(string roomCode)
    {
        PhotonNetwork.JoinRoom(roomCode.ToUpper());
    }

    #endregion

    #region Leave Room

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    #endregion
}
