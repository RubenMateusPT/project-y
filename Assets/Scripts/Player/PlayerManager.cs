﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Serialization;

public class PlayerManager : MonoBehaviour
{ 
    public string nickname;
    public string lastBoardTile;

    public int points = 0;

    public void AwardPoints(int points)
    {
        this.points += points;
    }

    public void TakePoints(int points)
    {
        if (this.points + points < 0)
        {
            this.points = 0;
        }
        else
        {
            this.points -= points;
        }
    }
}
