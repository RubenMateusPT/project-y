﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject playerPrefab;

    public PlayerManager[] players;

    public int lastPlayerTurn = -1;

    public PlayerManager[] GetPlayers()
    {
        return players;
    }

    public static GameManager Instance { get; set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(this.gameObject);

        SceneManager.sceneLoaded += (arg0, mode) => { OnSceneLoad(); };
        
        if(players[0].nickname != string.Empty){return;}

        int i = 0;
        foreach (var player in PhotonNetwork.CurrentRoom.Players.OrderBy(p => p.Value.ActorNumber))
        {
            players[i].nickname = player.Value.NickName;
            players[i].points = 0;
            players[i].lastBoardTile = string.Empty;
            i++;
        }
    }

    private void OnSceneLoad()
    {
        var board = FindObjectOfType<BoardManager>();

        if (board == null)
        {
            return;
        }

        for (int i = 0; i < players.Length; i++)
        {
            if (players[i].lastBoardTile != string.Empty)
            {
                board.RestoreTokensPosition(i,players[i].lastBoardTile);
            }
        }
    }

    public void SendPointsToPlayer(int points, int player)
    {
        if (points <= 0)
        {
            players[player].TakePoints(points);
        }
        else
        {
            players[player].AwardPoints(points);
        }
    }

    public void SendPointsToPlayers(int points)
    {
        foreach (var player in players)
        {
            if (points <= 0)
            {
                player.TakePoints(points);
            }
            else
            {
                player.AwardPoints(points);
            }
        }
    }


}
