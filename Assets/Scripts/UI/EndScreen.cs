﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreen : MonoBehaviourPunCallbacks
{
    [SerializeField] private Text scoreBoardText;

    [SerializeField] private AudioClip endClip;

    private void Start()
    {
        SoundManager.Instance.PlayMusic(endClip);

        scoreBoardText.text = string.Empty;
        foreach (var player in GameManager.Instance.GetPlayers().OrderByDescending(p => p.points))
        {
            scoreBoardText.text += $"{player.nickname} - {player.points}\n";
        }
    }

    public void ReturnToMainMenu()
    {
        foreach (var player in GameManager.Instance.players)
        {
            player.nickname = string.Empty;
            player.lastBoardTile = string.Empty;
            player.points = 0;
        }

        GameManager.Instance = new GameManager();

        this.gameObject.SetActive(false);
        
        NetworkManager.Instance.LeaveRoom();

        SceneManager.LoadScene(0);
    }
}
