﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviourPunCallbacks
{
    [SerializeField] private Text roomCodeText;
    [SerializeField] private InputField roomCodeInputField;
    [SerializeField] private Text playersListText;
    [SerializeField] private Button hostStartButton;
    [SerializeField] private GameObject usernameWindow;
    [SerializeField] private InputField usernameInputfield;

    [SerializeField] private AudioSource audioSource;
    [SerializeField] private AudioClip buttonPressedAudioClip;

    private NetworkManager networkManager;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        networkManager = NetworkManager.Instance;
        networkManager.ConnectToServer();

        if (string.IsNullOrEmpty(PlayerPrefs.GetString("Username")))
        {
            //usernameWindow.SetActive(true);
        }
        else
        {
            usernameInputfield.placeholder.GetComponent<Text>().text = PlayerPrefs.GetString("Username");
        }
    }

    public void SaveUsername()
    {
        if(usernameInputfield.text == string.Empty){return;}
        PlayerPrefs.SetString("Username",usernameInputfield.text);
        PhotonNetwork.LocalPlayer.NickName = PlayerPrefs.GetString("Username");
        usernameInputfield.placeholder.GetComponent<Text>().text = PlayerPrefs.GetString("Username");
        usernameInputfield.text = string.Empty;
    }

    public void CreateGame()
    {
        PhotonNetwork.LocalPlayer.NickName = PlayerPrefs.GetString("Username");
        networkManager.CreateRoom();
    }

    public void JoinGame()
    {
        PhotonNetwork.LocalPlayer.NickName = PlayerPrefs.GetString("Username");
        networkManager.JoinRoom(roomCodeInputField.text);
    }

    public void StartGame()
    {
        PhotonNetwork.LoadLevel(1);
    }

    public void LeaveRoom()
    {
        networkManager.LeaveRoom();
    }

    public override void OnJoinedRoom()
    {
        UpdateRoomCode();
        UpdatePlayerList();
    }

    public override void OnLeftRoom()
    {
        UpdateStartGame();
    }

    public override void OnPlayerEnteredRoom(Photon.Realtime.Player newPlayer)
    {
        UpdatePlayerList();
        UpdateStartGame();
    }

    public override void OnPlayerLeftRoom(Photon.Realtime.Player otherPlayer)
    {
        UpdatePlayerList();
        UpdateStartGame();
    }

    private void UpdateRoomCode()
    {
        roomCodeText.text = PhotonNetwork.CurrentRoom.Name;
    }

    private void UpdatePlayerList()
    {
        playersListText.text = string.Empty;
        foreach (Photon.Realtime.Player player in PhotonNetwork.PlayerList.OrderBy(p => p.ActorNumber))
        {
            playersListText.text += $"{player.NickName}\n";
        }
    }

    private void UpdateStartGame()
    {
        if (PhotonNetwork.IsMasterClient && PhotonNetwork.CurrentRoom.PlayerCount >= 2)
        {
            hostStartButton.gameObject.SetActive(true);
        }
        else
        {
            hostStartButton.gameObject.SetActive(false);
        }
    }

    public void ExitApplication()
    {
        Debug.Log("Closed App");
        Application.Quit();
    }

    public void OnButtonPress()
    {
        audioSource.PlayOneShot(buttonPressedAudioClip);
    }
}
