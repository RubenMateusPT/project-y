﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class CakeMinigameUI : MonoBehaviour
{ 
    public GameObject Player1Canvas, Player2Canvas;

    private void Start()
    {
        if (GameManager.Instance.GetPlayers()[GameManager.Instance.lastPlayerTurn].nickname ==
            PhotonNetwork.LocalPlayer.NickName)
        {
            Player1Canvas.SetActive(true);
        }
        else
        {
            Player2Canvas.SetActive(true);
        }
    }

}
