﻿using System;
using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class CakeMinigame : MonoBehaviourPun
{
    Color ToUnityColor(string color)
    {
        switch (color)
        {
            case "blue":
                return Color.blue;

            case "red":
                return Color.red;

            case "yellow":
                return Color.yellow;
        }

        return Color.white;
    }

    [SerializeField] private AudioClip[] correctSoundsClips;
    [SerializeField] private AudioClip incorrectSound;
    [SerializeField] private Image[] cakeToppingsImage;
    [SerializeField] private GameObject toppingPrefab;
    [SerializeField] private GameObject cakeBase;

    [SerializeField] private int winningPoints;
    [SerializeField] private int losePoints;

    [SerializeField] private GameObject winScreen, loseScreen;

    private GameObject[] addedToppings;

    private AudioSource audioSource;
    private CakeMinigameUI ui;

    private int currentTopping = 0;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        ui = GetComponent<CakeMinigameUI>();
    }

    private void Start()
    {
        addedToppings = new GameObject[cakeToppingsImage.Length];

        if (!PhotonNetwork.IsMasterClient)
        {
            return;
        }

        foreach (var cakeTopping in cakeToppingsImage)
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    cakeTopping.color = Color.red;
                    break;
                case 1:
                    cakeTopping.color = Color.yellow;
                    break;
                case 2:
                    cakeTopping.color = Color.blue;
                    break;
            }
        }

        photonView.RPC("SyncCake", RpcTarget.All, ConvertColorsToRPC());
    }

    private string[] ConvertColorsToRPC()
    {
        string[] colors = new string[3];

        int i = 0;
        foreach (var topping in cakeToppingsImage)
        {
            if (topping.color == Color.red)
            {
                colors[i] = "red";
            }
            else if(topping.color == Color.yellow)
            {
                colors[i] = "yellow";
            }
            else if (topping.color == Color.blue)
            {
                colors[i] = "blue";
            }

            i++;
        }

        return colors;
    }

    [PunRPC]
    public void SyncCake(string[] stringArray)
    {
        int i = 0;
        foreach (var topping in cakeToppingsImage)
        {
            topping.color = ToUnityColor(stringArray[i]);
            i++;
        }
    }

    public void AddToppingToCake(string toppping)
    {
        photonView.RPC("UpdateCakeOnServer", RpcTarget.All, toppping);
    }

    [PunRPC]
    public void UpdateCakeOnServer(string toppping)
    {
        var toppingColor = ToUnityColor(toppping);

        if (toppingColor != cakeToppingsImage[currentTopping].color)
        {
            audioSource.PlayOneShot(incorrectSound);
            StartCoroutine(FinishMinigame(false));
            return;
        }

        var newPos = currentTopping == 0
            ? cakeBase.transform.position + new Vector3(0, 0.2f, 0)
            : addedToppings[currentTopping - 1].transform.position + new Vector3(0, 0.2f, 0);

        var newTopping = Instantiate(toppingPrefab, newPos, Quaternion.identity);
        newTopping.GetComponent<MeshRenderer>().material.color = toppingColor;
        addedToppings[currentTopping] = newTopping;

        audioSource.PlayOneShot(correctSoundsClips[currentTopping]);
        currentTopping++;

        if (currentTopping >= cakeToppingsImage.Length)
        {
            StartCoroutine(FinishMinigame(true));
        }
    }



    public IEnumerator FinishMinigame(bool won)
    {
        ui.Player1Canvas.SetActive(false);

        if (won)
        {
            winScreen.SetActive(true);
            GameManager.Instance.SendPointsToPlayers(winningPoints);
        }
        else
        {
            loseScreen.SetActive(true);
            GameManager.Instance.SendPointsToPlayers(-losePoints);
        }

        yield return new WaitForSeconds(2);
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(1);
        }
    }
}
