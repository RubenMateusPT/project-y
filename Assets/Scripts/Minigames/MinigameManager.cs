﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinigameManager : MonoBehaviour
{
    [SerializeField]private AudioClip audioSource;

    private void Start()
    {
        SoundManager.Instance.PlayMusic(audioSource);
    }
}
