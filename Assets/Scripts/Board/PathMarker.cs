﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PathMarker : MonoBehaviour
{
    public Button playerChoiceButton;

    private List<BoardTile> path;

    public void SetThisMarkerPath(List<BoardTile> path)
    {
        this.path = path;
    }

    public List<BoardTile> GetMarkerPath()
    {
        return path;
    }
}
