﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class BoardTile : MonoBehaviour
{
    [SerializeField] private BoardTile[] nextBoardTiles;

    private bool isVisited = false;

    #region Depth First Search

    public List<List<BoardTile>> GetAvailablePaths(int diceRoll)
    {
        List<List<BoardTile>> paths = new List<List<BoardTile>>();
        List<BoardTile> path = new List<BoardTile>();

        path.Add(this);

        AddPathToPaths(this, this.isVisited, path, paths, diceRoll);

        return paths;
    }

    private void AddPathToPaths(BoardTile source, bool isVisited, List<BoardTile> path, List<List<BoardTile>> paths, int movesAvailable)
    {

        if (source.nextBoardTiles.Length <= 0 || path.Count > movesAvailable)
        {
            paths.Add(path.ToList());
            return;
        }

        isVisited = true;

        foreach (var nextBoardTile in source.nextBoardTiles)
        {
            if (!nextBoardTile.isVisited)
            {
                path.Add(nextBoardTile);

                AddPathToPaths(nextBoardTile, isVisited, path, paths, movesAvailable);

                path.Remove(nextBoardTile);
            }
        }

        isVisited = false; //resets tile flag
    }

    #endregion

    
}
