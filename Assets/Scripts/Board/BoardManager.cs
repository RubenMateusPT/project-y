﻿using Photon.Pun;
using System.Collections;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviourPunCallbacks
{
    [SerializeField] private Token[] boardTokens;

    [SerializeField] private GameObject pathChoiceMarkerPrefab;

    [SerializeField] private AudioClip boardThemeAudioClip;
    [SerializeField] private AudioClip diceRollClip;

    private int playerTurn;

    private BoardUI boardUI;
    private Dice dice;
    private AudioSource audioSource;
    private Camera mainCamera;
    [SerializeField] private Vector3[] cameraOnToken;
    [SerializeField] private Vector3[] cameraOnBoard;

    private void Awake()
    {
        boardUI = GetComponent<BoardUI>();
        dice = GetComponent<Dice>();
        audioSource = GetComponent<AudioSource>();
        mainCamera = Camera.main;
    }

    private void Start()
    {
        SoundManager.Instance.PlayMusic(boardThemeAudioClip);

        if (GameManager.Instance.lastPlayerTurn != -1)
        {
            playerTurn = GameManager.Instance.lastPlayerTurn;
            EndPlayerTurn();
            return;
        }

        if (PhotonNetwork.IsMasterClient)
        {
            photonView.RPC("ChooseFirstPlayer", RpcTarget.All, Random.Range(0, 2));
        }
    }

    [PunRPC]
    private void ChooseFirstPlayer(int player)
    {
        playerTurn = player;
        UpdatePlayerTurnUI();
    }

    public void StartPlayerTurn()
    {
        boardUI.playerTurn.SetActive(false);

        var diceRoll = dice.RollDice();
        photonView.RPC("ProcessPlayerTurn", RpcTarget.All, diceRoll);
    }

    [PunRPC]
    private void ProcessPlayerTurn(int diceRoll)
    {
        audioSource.PlayOneShot(diceRollClip);

        boardUI.RollInfo.SetActive(true);
        boardUI.rolledText.text = diceRoll.ToString();

        var tokenToPlay = boardTokens[playerTurn];
        var possiblePaths = tokenToPlay.GetCurrentBoardTile().GetAvailablePaths(diceRoll);

        UpdateCamera(cameraOnBoard);

        foreach (var path in possiblePaths)
        {
            if (diceRoll >= path.Count)
            {
                StartCoroutine(NoMovesAvailable());
                return;
            }

            if (!IsMyTurn()) { return; }

            var lastBoardTileOfPath = path.Last();

            var markerPathObject = Instantiate(pathChoiceMarkerPrefab);
            markerPathObject.transform.position = lastBoardTileOfPath.transform.position;
            var markerPath = markerPathObject.GetComponent<PathMarker>();
            markerPath.transform.position = lastBoardTileOfPath.transform.position;
            markerPath.SetThisMarkerPath(path);
            markerPath.playerChoiceButton.onClick.AddListener(delegate { StartPlayerMove(markerPath); });
        }
    }

    private void StartPlayerMove(PathMarker pathMarker)
    {

        var markerPaths = FindObjectsOfType<PathMarker>();
        foreach (var markerPath in markerPaths)
        {
            Destroy(markerPath.gameObject);
        }
        boardUI.RollInfo.SetActive(false);

        var pathChoosen = pathMarker.GetMarkerPath().ToList();

        photonView.RPC("ProcessPlayerMove", RpcTarget.All, RCPHelper.Serialize(pathChoosen));
    }

    [PunRPC]
    private void ProcessPlayerMove(string[] serializedList)
    {
        UpdateCamera(cameraOnToken);

        var pathChoosen = RCPHelper.DeSerialize(serializedList);
        var tokenToPlay = boardTokens[playerTurn];



        tokenToPlay.StartMoving(pathChoosen);
    }

    public void StartTileAction()
    {
        var tokenToPlay = boardTokens[playerTurn];
        var specialTile = tokenToPlay.GetCurrentBoardTile().gameObject.GetComponent<ISpecialTile>();

        GameManager.Instance.GetPlayers()[playerTurn].lastBoardTile = tokenToPlay.GetCurrentBoardTile().name;

        if (specialTile != null)
        {
            specialTile.DoTileAction(playerTurn);
        }
        else
        {
            EndPlayerTurn();
        }
    }

    private IEnumerator NoMovesAvailable()
    {
        yield return new WaitForSeconds(1);

        boardUI.boardMessageText.text = "No moves available!\n" +
                                        "Skipping Turn";
        boardUI.boardMessageText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);

        EndPlayerTurn();
    }

    public void StartLoadMiniGame(int minigameSceneIndex)
    {
        StartCoroutine(LoadMinigame(minigameSceneIndex));
    }

    private IEnumerator LoadMinigame(int minigame)
    {
        boardUI.boardMessageText.gameObject.SetActive(true);
        boardUI.boardMessageText.text = "Loading Minigame!";
        yield return new WaitForSeconds(2);

        GameManager.Instance.lastPlayerTurn = playerTurn;

        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(minigame);
        }
    }

    public void EndPlayerTurn()
    {
        if (playerTurn == 0)
        {
            playerTurn = 1;
        }
        else
        {
            playerTurn = 0;
        }

        boardUI.RollInfo.SetActive(false);
        boardUI.boardMessageText.gameObject.SetActive(false);

        UpdatePlayerTurnUI();
        UpdateCamera(cameraOnToken);

        if (boardUI != null)
        {
            boardUI.pOScore.text = $"{GameManager.Instance.players[0].nickname}\n{GameManager.Instance.players[0].points} Points";
            boardUI.pTScore.text = $"{GameManager.Instance.players[1].nickname}\n{GameManager.Instance.players[1].points} Points";
        }
    }

    private void UpdatePlayerTurnUI()
    {
        if (IsMyTurn())
        {
            boardUI.playerTurn.SetActive(true);
        }
        else
        {
            boardUI.playerTurn.SetActive(false);
        }
    }

    private bool IsMyTurn()
    {
        var players = GameManager.Instance.GetPlayers();
        var player = players[playerTurn];


        return player.nickname == PhotonNetwork.LocalPlayer.NickName;
    }

    public void RestoreTokensPosition(int player, string lastBoardTile)
    {
        var token = boardTokens[player];

        var boardTile = GameObject.Find(lastBoardTile).GetComponent<BoardTile>();

        token.SetCurrentBoardTile(boardTile);

        if (boardUI != null)
        {
            boardUI.pOScore.text = $"{GameManager.Instance.players[0].nickname}\n{GameManager.Instance.players[0].points} Points";
            boardUI.pTScore.text = $"{GameManager.Instance.players[1].nickname}\n{GameManager.Instance.players[1].points} Points";
        }
    }

    public void EndGame()
    {
        StartCoroutine(LoadScoreboardScene());
    }

    private IEnumerator LoadScoreboardScene()
    {
        boardUI.boardMessageText.text = "Congratulations!\n" +
                                        "Board completed :D";
        boardUI.boardMessageText.gameObject.SetActive(true);

        yield return new WaitForSeconds(3);
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonNetwork.LoadLevel(3);
        }
    }

    private void UpdateCamera(Vector3[] cameraType)
    {
        var tokenToPlay = boardTokens[playerTurn];

        mainCamera.gameObject.transform.parent = tokenToPlay.transform;

        mainCamera.gameObject.transform.localPosition = cameraType[0];

        mainCamera.gameObject.transform.localEulerAngles = cameraType[1];
    }
}
