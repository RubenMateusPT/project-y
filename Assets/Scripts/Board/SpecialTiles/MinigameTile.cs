﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class MinigameTile : MonoBehaviour,ISpecialTile
{
    [SerializeField] private int minigameSceneIndex;

    public void DoTileAction(int player)
    {
        FindObjectOfType<BoardManager>().StartLoadMiniGame(minigameSceneIndex);
    }

}
