﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PenaltyTile : MonoBehaviour, ISpecialTile
{
    [SerializeField] private AudioClip penaltyClip;
    [SerializeField] private GameObject tileMessagePrefab;
    [SerializeField] private int penaltyPoints = 10;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void DoTileAction(int player)
    {
        var tileMessage = Instantiate(tileMessagePrefab, transform.position, Quaternion.identity);

        var text = tileMessage.GetComponentInChildren<Text>();

        text.text = $"-{penaltyPoints}\nPoints";
        text.color = Color.red;

        audioSource.PlayOneShot(penaltyClip);

        Destroy(tileMessage, 3);

        FindObjectOfType<BoardManager>().EndPlayerTurn();
        GameManager.Instance.SendPointsToPlayer(-penaltyPoints,player);
    }
}
