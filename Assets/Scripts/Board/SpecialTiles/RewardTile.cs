﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RewardTile : MonoBehaviour, ISpecialTile
{
    [SerializeField] private AudioClip rewardSound;
    [SerializeField] private GameObject tileMessagePrefab;
    [SerializeField] private int rewardPoints = 10;

    private AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void DoTileAction(int player)
    {
        var tileMessage = Instantiate(tileMessagePrefab, transform.position, Quaternion.identity); ;

        var text = tileMessage.GetComponentInChildren<Text>();

        text.text = $"+{rewardPoints}\nPoints";
        text.color = Color.green;

        audioSource.PlayOneShot(rewardSound);

        Destroy(tileMessage,3);

        GameManager.Instance.SendPointsToPlayer(rewardPoints,player);
        FindObjectOfType<BoardManager>().EndPlayerTurn();
    }
}
