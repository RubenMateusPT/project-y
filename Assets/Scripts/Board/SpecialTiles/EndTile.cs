﻿using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

public class EndTile : MonoBehaviourPunCallbacks,ISpecialTile
{
    private List<int> playersInFinishLine = new List<int>();

    public void DoTileAction(int player)
    {
        playersInFinishLine.Add(player);

        if (playersInFinishLine.Count == 2)
        {
            FindObjectOfType<BoardManager>().EndGame();
            return;
        }

        FindObjectOfType<BoardManager>().EndPlayerTurn();
    }
}
