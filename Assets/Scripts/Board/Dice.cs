﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dice : MonoBehaviour
{
    [SerializeField] private int sides = 6;

    public int RollDice()
    {
        return Random.Range(1, sides+1);
    }

    public int RollDice(int sides)
    {
        return Random.Range(1, sides+1);
    }
}
