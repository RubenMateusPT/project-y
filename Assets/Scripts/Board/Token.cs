﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Token : MonoBehaviour
{
    [SerializeField] private BoardTile startingTile;
    [SerializeField] private float movementSpeed;

    [SerializeField] private Animator animator;



    private BoardTile currentBoardTile;
    public BoardTile GetCurrentBoardTile()
    {
        return currentBoardTile;
    }

    public void SetCurrentBoardTile(BoardTile boardTile)
    {
        currentBoardTile = boardTile;
        this.transform.position = currentBoardTile.transform.position;
    }

    private void Start()
    {
        if(currentBoardTile != null){return;}
        currentBoardTile = startingTile;
    }

    public void StartMoving(List<BoardTile> path)
    {
        StartCoroutine(Move(path));
    }

    private IEnumerator Move(List<BoardTile> path)
    {
        animator.SetBool("walk",true);

        foreach (var boardTile in path)
        {
            yield return StartCoroutine(SmoothMove(boardTile));
        }

        animator.SetBool("walk", false);

        FindObjectOfType<BoardManager>().StartTileAction();
    }

    IEnumerator SmoothMove(BoardTile nextBoardTile)
    {
        while (Vector3.Distance(this.transform.position, nextBoardTile.transform.position) > movementSpeed * Time.deltaTime)
        {
            UpdateCharacterRotation(nextBoardTile);

            this.transform.position = Vector3.MoveTowards(this.transform.position, nextBoardTile.transform.position, movementSpeed * Time.deltaTime);
            yield return null;
        }
        this.transform.position = nextBoardTile.transform.position;
        currentBoardTile = nextBoardTile;
    }

    private void UpdateCharacterRotation(BoardTile nextBoardTile)
    {
        if (nextBoardTile.transform.position.x - transform.position.x > 0)
        {
            transform.localEulerAngles = new Vector3(0, 0, 0);
        }
        else if (nextBoardTile.transform.position.x - transform.position.x < 0)
        {
            transform.localEulerAngles = new Vector3(0, 180, 0);
        }

        if (nextBoardTile.transform.position.z - transform.position.z > 0)
        {
            transform.localEulerAngles = new Vector3(0, -90, 0);
        }
        else if (nextBoardTile.transform.position.z - transform.position.z < 0)
        {
            transform.localEulerAngles = new Vector3(0, 90, 0);
        }
    }


}
