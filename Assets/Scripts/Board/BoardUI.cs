﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardUI : MonoBehaviour
{
     public GameObject playerTurn, RollInfo;
     public Text rolledText;
     public Text boardMessageText;
     public Text pOScore, pTScore;
}
