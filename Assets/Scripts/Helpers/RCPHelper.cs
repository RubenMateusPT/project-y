﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RCPHelper : MonoBehaviour
{
    public static string[] Serialize(List<BoardTile> boardTiles)
    {
        string[] objNames = new string[boardTiles.Count];

        for (int i = 0; i < boardTiles.Count; i++)
        {
            objNames[i] = boardTiles[i].name;
        }

        return objNames;
    }

    public static List<BoardTile> DeSerialize(string[] serializedObject)
    {
        var path = new List<BoardTile>();

        foreach (var objId in serializedObject)
        {
            var go = GameObject.Find(objId);
            path.Add(go.GetComponent<BoardTile>()); ;
        }

        return path;
    }
}
